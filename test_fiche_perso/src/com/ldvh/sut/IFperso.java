package com.ldvh.sut;
import java.util.ArrayList;

import com.ldvh.section_bouchon.ISection;
import com.ldvh.objet_bouchont.IListObj;
import com.ldvh.objet_bouchont.IObj;

public interface IFperso {
	public void ajouterObjet(IObj objet);
	public void supprimerObjet(IObj objet);
	public IListObj listeObjet();
	public void changerSection(ISection section);
	public ArrayList<ISection> estPassePar(ISection section);
	public ISection sectionActu();
}
