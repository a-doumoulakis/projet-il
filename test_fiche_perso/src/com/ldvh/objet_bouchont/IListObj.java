
package com.ldvh.objet_bouchont;

import java.util.ArrayList;

public interface IListObj {
	public void ajoutObjet(IObj objet);
	public void supprObjet(IObj objet);
	public ArrayList<IObj> listeObjet();
	public int getSize();
	public int getNextId();
	public IObj getObjetByName(String nom);
	public IObj getObjetById(int id);
}