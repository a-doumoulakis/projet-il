package com.ldvh.objet_bouchont;

/**
 * Created by tassos on 12/7/15.
 */
public class Mock_Obj implements IObj{

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public String getNom() {
        return "Mock_obj_name";
    }

    @Override
    public String getDescription() {
        return "Mock_obj_desc";
    }
}
