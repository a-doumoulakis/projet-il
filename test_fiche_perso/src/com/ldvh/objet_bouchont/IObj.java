package com.ldvh.objet_bouchont;

public interface IObj {
	
	public int getId();
	public String getNom();
	public String getDescription();

}
