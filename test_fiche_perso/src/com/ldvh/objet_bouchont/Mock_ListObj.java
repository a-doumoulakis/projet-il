package com.ldvh.objet_bouchont;

import java.util.ArrayList;

/**
 * Created by tassos on 12/7/15.
 */
public class Mock_ListObj implements IListObj {
    @Override
    public void ajoutObjet(IObj objet) {
        System.out.println("Mock ajout");
    }

    @Override
    public void supprObjet(IObj objet){
        System.out.println("Mock suppr");
    }

    @Override
    public ArrayList<IObj> listeObjet() {
        ArrayList<IObj> res = new ArrayList<>();
        res.add(new Mock_Obj());
        return res;
    }

    @Override
    public int getSize() {
        return -1;
    }

    @Override
    public int getNextId() {
        return -1;
    }

    @Override
    public IObj getObjetByName(String nom) {
        return new Mock_Obj();
    }

    @Override
    public IObj getObjetById(int id) {
        return new Mock_Obj();
    }
}
