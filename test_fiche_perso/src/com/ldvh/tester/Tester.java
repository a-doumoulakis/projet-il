package com.ldvh.tester;

import com.ldvh.objet_bouchont.IListObj;
import com.ldvh.objet_bouchont.Mock_Obj;
import com.ldvh.sut.FichePersonnage;
import com.ldvh.sut.IFperso;

/**
 * Created by tassos on 12/7/15.
 * in {$PWD}
 */
public class Tester {

     public static boolean test(){
        IFperso sut = new FichePersonnage("test", "test");
        sut.ajouterObjet(new Mock_Obj());
        sut.supprimerObjet(new Mock_Obj());
        IListObj list_test = sut.listeObjet();
        return list_test == null;
        /*
        *
        * test avec les isection nécéssite un composant bouchon section
        *
        * */
    }

    public static void main(){
        System.out.println(Tester.test());
    }
}