# README #


### What is this repository for? ###

* Contient les diagrammes et en règle générale le modèle du projet.
* V0.2

### How do I get set up? ###
* Avant de faire un pull il faut aller dans "Manage account" et ajouter sa clé ssh publique

* Suivre les étapes qui suivent pour créer son git :
 - mkdir /path/to/your/project
 - cd /path/to/your/project
 - git init
 - git remote add origin master git@bitbucket.org:scathSTB/projet-il.git


### Who do I talk to? ###

* https://groups.google.com/forum/#!forum/ingnierie-logicielle-

### Ce qui est dans le projet pour le moment ###

* Diagramme des classes metiers

* Diagramme use case 

* Fiches detaillées : 
      Exporter,  
      Gérer Enchainement,  
      Gérer Livre, 
      Gérer Objet,  
      Gérer Section,  
      Enregistrer,
      Gérer Condition
   
* Diagrammes de sequences : 
      Gérer Livre (SN, A1, A3),  
      Gérer Condition (SN),
      Gérer Section (SN, A3),
      Gérer Enchainement (SN, A1),
      Gérer Objet (SN)
