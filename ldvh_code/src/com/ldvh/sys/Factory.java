package com.ldvh.sys;

import com.ldvh.deroulement.Condition;
import com.ldvh.deroulement.Enchainement;
import com.ldvh.deroulement.GestDeroulement;
import com.ldvh.deroulement.ICondition;
import com.ldvh.deroulement.IDeroulement;
import com.ldvh.deroulement.IEnchainement;
import com.ldvh.deroulement.ISection;
import com.ldvh.deroulement.ISections;
import com.ldvh.deroulement.Section;
import com.ldvh.deroulement.Sections;
import com.ldvh.f_perso.FichePersonnage;
import com.ldvh.f_perso.IFperso;
import com.ldvh.livre.ILivre;
import com.ldvh.livre.Livre;
import com.ldvh.objet.GestObjet;
import com.ldvh.objet.IListObj;
import com.ldvh.objet.IObj;

public class Factory {
	
	public static IObj newObj(){
		return (IObj) new Object();
	}
	
	public static IListObj newListObj(){
		return (IListObj) new GestObjet();
	}
	
	public static ILivre newLivre(){
		return (ILivre) new Livre();
	}
	
	public static IFperso newFichePersonnage(String nom, String description){
		return (IFperso) new FichePersonnage(nom, description);
	}
	
	public ICondition newCondition(){
		return (ICondition) new Condition(null, null);
	}
	
	public IDeroulement newDeroulement(){
		return (IDeroulement) new GestDeroulement();
	}
	
	public IEnchainement newEnchainement(ISection source, ISection destination, String texte){
		return (IEnchainement) new Enchainement((Section)source, (Section)destination, texte);
	}
	
	public ISection newSection(String titre, String description){
		return (ISection)new Section(titre, description);
	}
	
	public ISections newSections(){
		return (ISections)new Sections();
	}

}
