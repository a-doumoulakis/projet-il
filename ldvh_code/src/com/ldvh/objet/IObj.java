package com.ldvh.objet;

public interface IObj {
	
	public int getId();
	public String getNom();
	public String getDescription();

}
