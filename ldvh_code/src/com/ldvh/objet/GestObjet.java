package com.ldvh.objet;

import java.util.ArrayList;

public class GestObjet implements IListObj {
	private ArrayList<IObj> liste_obj;
	private static int ids = 0;
	
	@Override
	public IObj getObjetByName(String nom) {
		for(IObj e : this.liste_obj){
			if(e.getNom().equals(nom)){
				return e;
			}
		}
		return null;
	}
	
	@Override
	public IObj getObjetById(int id) {
		for(IObj e : this.liste_obj){
			if(e.getId() == id){
				return e;
			}
		}
		return null;
	}
	
	@Override
	public int getNextId(){
		return ++ids;
	}
	@Override
	public void ajoutObjet(IObj objet) {
		this.liste_obj.add(objet);
	}
	@Override
	public void supprObjet(IObj objet){
		this.liste_obj.remove(objet);
	}
	@Override
	public ArrayList<IObj> listeObjet() {
		return this.liste_obj;
	}
	@Override
	public int getSize() {
		return this.liste_obj.size();
	}

}