
package com.ldvh.objet;

public class Objet implements IObj{
	private final int id;
	private String nom;
	private String description;
	private GestObjet gestionnaire;
	
	public Objet(String nom, String desc, GestObjet gest){
		this.gestionnaire = gest;
		this.nom = nom;
		this.description = desc;
		this.id = this.gestionnaire.getNextId();
	}

	public int getId(){
		return this.id;
	}
	
	public String getNom(){
		return this.nom;
	}
	
	public String getDescription(){
		return this.description;
	}
}