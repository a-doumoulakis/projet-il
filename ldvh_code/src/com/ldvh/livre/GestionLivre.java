/**
 * 
 */
package com.ldvh.livre;


import java.io.File;
import java.util.Set;

import com.ldvh.deroulement.ICondition;
import com.ldvh.deroulement.IDeroulement;
import com.ldvh.deroulement.IEnchainement;
import com.ldvh.deroulement.ISection;
import com.ldvh.objet.IListObj;
import com.ldvh.objet.IObj;

/** 
 * <!-- begin-UML-doc -->
 * <!-- end-UML-doc -->
 * @author 3100971
 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
 */
public class GestionLivre implements ILivre{
	/** 
	 * <!-- begin-UML-doc -->
	 * <!-- end-UML-doc -->
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Livre livre;
	/** 
	 * <!-- begin-UML-doc -->
	 * <!-- end-UML-doc -->
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Set<IObj> liste_objet;
	/** 
	 * <!-- begin-UML-doc -->
	 * <!-- end-UML-doc -->
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	private Set<ISection> sections;

	/** 
	 * (non-Javadoc)
	 * @see ILivre#chargerLivre(String nomFichier)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void chargerLivre(String nomFichier) {
		// begin-user-code
		livre = parseFile(new File(nomFichier));
		// end-user-code
	}

	private Livre parseFile(File file) {
		// TODO Module de remplacement de méthode auto-généré
		return null;
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#getTitre()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public String getTitre() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return livre.getTitre();
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#getSections()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Set<ISection> getSections() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return sections;
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#getAuteur()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public String getAuteur() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return livre.getAuteur();
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#getRepertoire()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public String getRepertoire() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return livre.getRepertoire();
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#setTitre(String titre)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setTitre(String titre) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		livre.setTitre(titre);
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#setAuteur(String nom)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setAuteur(String nom) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		livre.addAuteur(nom);
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#setRepertoire(String chemin)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void setRepertoire(String chemin) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		livre.setRepertoire(chemin);
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#exporterPDF()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void exporterPDF() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see ILivre#exporterHTML()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void exporterHTML() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#ajoutEnchainnement()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void ajoutEnchainnement() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#ajouterCondtion()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void ajouterCondtion() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#ajoutSection()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void ajoutSection() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#definirCondition(String typeCondition, Boolean autreCondition)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void definirCondition(String typeCondition, Boolean autreCondition) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#modifierSection()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void modifierSection() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#modifierEnchainement()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void modifierEnchainement() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#validerAjoutSection(String titre, String descption, Objet liste_objet)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public boolean validerAjoutSection(String titre, String descption,
			IObj liste_objet) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return false;
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#validerModifEnchainement(Enchainement ench02, Section dep, Section arrivée, String titre, String desc, Condition cond)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public boolean validerModifEnchainement(IEnchainement ench02, ISection dep,
			ISection arrivée, String titre, String desc, ICondition cond) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return false;
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#validerModifierSection(Section sec, String titre, String desc, Objet liste_objet)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public boolean validerModifierSection(ISection sec, String titre,
			String desc, IListObj liste_objet) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return false;
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IDeroulement#validerAjoutEnchainnement(Section dep, Section arr, String titre, String desc, Condition condition)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public boolean validerAjoutEnchainnement(ISection dep, ISection arr,
			String titre, String desc, ICondition condition) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return false;
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IObj#ajoutObjet(int objetId)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void ajoutObjet(int objetId) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IObj#supprObjet(int objetId)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public void supprObjet(int objetId) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré

		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IObj#listeObjet()
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public Set<IObj> listeObjet() {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return null;
		// end-user-code
	}

	/** 
	 * (non-Javadoc)
	 * @see IObj#validerAjoutObjet(String nom, String desc)
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */
	public boolean validerAjoutObjet(String nom, String desc) {
		// begin-user-code
		// TODO Module de remplacement de méthode auto-généré
		return false;
		// end-user-code
	}
}