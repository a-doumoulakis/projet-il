package com.ldvh.deroulement;
/**
 * 
 */

import java.util.ArrayList;


public class Sections implements ISections {
	
	private ArrayList<Section> sections;

	public Sections(){
		sections = new ArrayList<Section>();
	}

	public Section getSection(int Id) {
		for(Section s: sections){
			if (s.getId() == Id){
				return s;
			}
		}
		return null;
	}

	public void supprSection(Section sect) {
		sections.remove(sect);
	}

	public void ajouterSection(Section sect) {
		sections.add(sect);
	}

	public ArrayList<Section> getSections(){
		return sections;
	}
}