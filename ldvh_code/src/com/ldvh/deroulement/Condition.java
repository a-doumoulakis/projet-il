package com.ldvh.deroulement;
/**
 * 
 */

import java.util.ArrayList;
import java.util.Arrays;

import com.ldvh.objet.IObj;


public class Condition implements ICondition {
	/** 
	 * <!-- begin-UML-doc -->
	 * <!-- end-UML-doc -->
	 * @generated "UML vers Java (com.ibm.xtools.transform.uml2.java5.internal.UML2JavaTransform)"
	 */

	private ArrayList<IObj> objet;

	private ArrayList<Section> section;

	public Condition(IObj[] ensObjet, Section[] ensSection ){
		objet = new ArrayList<IObj>(Arrays.asList(ensObjet));
		section = new ArrayList<Section>(Arrays.asList(ensSection));
	}

	public ArrayList<IObj> getObjets() {
		return this.objet;
	}

	public void supprObjet(IObj obj) {
		objet.remove(obj);
	}

	public void ajouterObjet(IObj obj) {
		objet.add(obj);
	}

	public ArrayList<Section> getSections() {
		return section;
	}

	public void supprSection(Section sect) {
		section.remove(sect);
	}

	public void ajouterSection(Section sect) {
		section.add(sect);
	}
	
}