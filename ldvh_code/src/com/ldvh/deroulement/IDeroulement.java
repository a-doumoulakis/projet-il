package com.ldvh.deroulement;

import com.ldvh.objet.IObj;


public interface IDeroulement {

	public void ajoutEnchainnement(Section sect1, Section sect2, String titre,
			String desc);

	
	public void ajouterCondtion(Enchainement ench, IObj Objets,
			Sections sections);

	public void ajoutSection(String titre, String texte, IObj objets);

	public void definirCondition(String typeCondition, Boolean autreCondition);

	public void modifierSection(Integer idSect, String titre, String texte);

	public void modifierEnchainement(Integer idEnch, String titre, String desc);

	public boolean validerAjoutSection(String titre, String descption,
			IObj liste_objet);

	public boolean validerModifEnchainement(Enchainement ench02, Section dep,
			Section arrivee, String titre, String desc, Condition cond);

	public boolean validerModifierSection(Section sec, String titre,
			String desc, IObj liste_objet);


	public boolean validerAjoutEnchainnement(Section dep, Section arr,
			String titre, String desc, Condition condition);
}