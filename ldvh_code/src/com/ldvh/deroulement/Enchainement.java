package com.ldvh.deroulement;

/**
 * 
 */


public class Enchainement implements IEnchainement {
	
	private Section source;
	
	private Section destination;
	
	private Condition condition;
	
	private String titre;

	private String description;
	
	private String id;

	private static int cpt = 0;
	
	public Enchainement(Section s, Section d, String t){
		source=s;
		destination=d;
		titre=t;
		id="sect"+(cpt++);
	}
	
	public String getTitre() {
		return titre;
	}

	public String getDescription() {
		return description;
	}

	public String getId() {
		return id;
	}

	public Section getSource() {
		return source;
	}

	public Section getDestination() {
		return destination;
	}

	public void setSource(Section sect) {
		source=sect;
	}


	public void setDestination(Section sect) {
		destination=sect;
	}
	
	public Condition getCondition(){
		return condition;
	}
	
	public void setCondition(Condition c){
		condition=c;
	}
	
}