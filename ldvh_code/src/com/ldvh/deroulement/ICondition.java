package com.ldvh.deroulement;
import java.util.ArrayList;

import com.ldvh.objet.IObj;

/**
 * 
 */


public interface ICondition {
	
	public ArrayList<IObj> getObjets();

	
	public void supprObjet(IObj obj);

	
	public void ajouterObjet(IObj obj);

	
	public ArrayList<Section> getSections();

	
	public void supprSection(Section sect);

	
	public void ajouterSection(Section sect);
}