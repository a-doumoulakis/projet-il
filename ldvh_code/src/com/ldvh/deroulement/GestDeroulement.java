package com.ldvh.deroulement;
/**
 * 
 */

import java.util.ArrayList;

import com.ldvh.objet.IObj;


public class GestDeroulement implements IDeroulement {
	

	public void ajoutEnchainnement(Section sect1, Section sect2, String titre,
			String desc) {
		Enchainement e= new Enchainement(sect1, sect2, titre);
	}

	public void ajouterCondtion(Enchainement ench, IObj Objets,
			Sections sections) {
		
	}

	public void ajoutSection(String titre, String texte, IObj objets) {
		
	}

	public void definirCondition(String typeCondition, Boolean autreCondition) {
		
	}

	public void modifierSection(Integer idSect, String titre, String texte) {
		
	}

	public void modifierEnchainement(Integer idEnch, String titre, String desc) {
		
	}

	public boolean validerAjoutSection(String titre, String descption,
			IObj liste_objet) {
		return true;
	}

	public boolean validerModifEnchainement(Enchainement ench02, Section dep,
			Section arrivee, String titre, String desc, Condition cond) {
		
		return true;
	}

	public boolean validerModifierSection(Section sec, String titre,
			String desc, IObj liste_objet) {
		return true;
	}

	public boolean validerAjoutEnchainnement(Section dep, Section arr,
			String titre, String desc, Condition condition) {
		return true;
	}
}