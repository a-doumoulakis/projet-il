package com.ldvh.deroulement;
/**
 * 
 */

import java.util.ArrayList;
import java.util.Arrays;
import com.ldvh.objet.IObj;

public class Section implements ISection {
	
	
	private ArrayList<Enchainement> enchainementsSuivant;
	
	private String titre;
	
	private String description;
	
	private ArrayList<IObj> objet;
	
	private static int id;
	
	private static int cpt=0;
	
	public Section(String titre, String description){
		this.titre=titre;
		this.description=description;
		objet= new ArrayList<IObj>();
		id=cpt;
		cpt++;
	}

	public Section(String titre, String description, IObj[] ensObjet ){
		this.titre=titre;
		this.description=description;
		objet = new ArrayList<IObj>(Arrays.asList(ensObjet));
		id=cpt;
		cpt++;
	}

	
	public void addObjet(IObj o){
		objet.add(o);
	}
	
	public void supprObjet(IObj o){
		objet.remove(o);
	}

	public ArrayList<IObj> getObjets(){
		return objet;
	}
	
	public void addEnchainement(Enchainement e){
		enchainementsSuivant.add(e);
	}
	
	public void supprEnchainement(Enchainement e){
		enchainementsSuivant.remove(e);
	}
	
	public void setTitre(String titre) {
		this.titre = titre;
	}

	public void setDesc(String desc) {
		description = desc;
	}

	public int getId() {
		return id;
	}

	public String getTitre() {
		return titre;
	}

	public String getDesc() {
		return description;
	}
}