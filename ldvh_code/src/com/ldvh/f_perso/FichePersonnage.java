package com.ldvh.f_perso;

import java.util.ArrayList;

import com.ldvh.deroulement.ISection;
import com.ldvh.objet.IListObj;
import com.ldvh.objet.IObj;

public class FichePersonnage implements IFperso{
	private String nom;
	private String description;
	private IListObj possede;
	private ISection section_actu;
	private ArrayList<ISection> passe_par;
	
	public FichePersonnage(String nom, String desc){
		this.nom = nom;
		this.description = desc;
		this.possede = null;
		this.passe_par = new ArrayList<ISection>();
	}
	
	@Override
	public void ajouterObjet(IObj Objet1){
		this.possede.ajoutObjet(Objet1);		
	}
	@Override
	public void supprimerObjet(IObj Objet1) {
		this.possede.supprObjet(Objet1);
	}
	@Override
	public IListObj listeObjet(){
		return this.possede;
	}
	
	@Override
	public void changerSection(ISection NouvSection) {
		this.passe_par.add(NouvSection);
		this.section_actu = NouvSection; 
		
	}
	
	@Override
	public ArrayList<ISection> estPassePar(ISection Section) {
		return this.passe_par;
	}
	
	@Override
	public ISection sectionActu() {
		return this.section_actu;
	}
}