package com.ldvh.f_perso;
import java.util.ArrayList;

import com.ldvh.deroulement.ISection;
import com.ldvh.objet.IListObj;
import com.ldvh.objet.IObj;

public interface IFperso {
	public void ajouterObjet(IObj objet);
	public void supprimerObjet(IObj objet);
	public IListObj listeObjet();
	public void changerSection(ISection section);
	public ArrayList<ISection> estPassePar(ISection section);
	public ISection sectionActu();
}
